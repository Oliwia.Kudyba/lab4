package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;
    int rows;
    int cols;

    public BriansBrain(int row, int column){
        currentGeneration = new CellGrid(row, column, CellState.DEAD);
		initializeCells();
		this.rows = row;
		this.cols = column;
    }
    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for(int row=0; row < numberOfRows(); row++){
			for(int col=0; col < numberOfColumns(); col++){
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
	currentGeneration = nextGeneration;
    }


    //En levende celle blir døende
    //En døende celle blir død
    //En død celle med akkurat 2 levende naboer blir levende
    //En død celle forblir død ellers



    @Override
    public CellState getNextCell(int row, int col) {
        CellState cellState = getCellState(row, col);
		int aliveNeightbors = countNeighbors(row, col, CellState.ALIVE);
		if ((cellState == cellState.ALIVE) && (aliveNeightbors < 2)){
			return cellState.DYING;
    	}
        else if((cellState == cellState.ALIVE)){
            return cellState.DYING;
        }
        else if((cellState == cellState.DYING)){
            return cellState.DEAD;
        }
        else if((cellState == cellState.DEAD) && (aliveNeightbors < 2)){
            return cellState.DEAD;
        }
        else if ((cellState == cellState.DEAD) && (aliveNeightbors == 2)){
            return cellState.ALIVE;
        }
        return cellState.DEAD;
    }


    private int countNeighbors(int row, int col, CellState state) {
		int neighborCount = 0;
		for (int r = row-1; r <= row+1; r++){
			for(int c = col-1; c<= col+1; c ++){
				if(r >= currentGeneration.numRows() || c >= currentGeneration.numColumns() || r < 0 || c < 0){
					continue;
				}
				if (r == row && c == col){
					continue;
				}
				else if (state == currentGeneration.get(r,c)){
					neighborCount ++;
				}
			}
		}
		return neighborCount;
	}

    @Override
    public int numberOfRows() {
        return this.rows;
    }

    @Override
    public int numberOfColumns() {
        return this.cols;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}