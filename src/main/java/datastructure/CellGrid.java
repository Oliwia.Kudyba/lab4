package datastructure;

import java.lang.Thread.State;

import javax.swing.SwingWorker.StateValue;

import cellular.CellState;

public class CellGrid implements IGrid {
    int cols;
    int rows;
    CellState[][] grid;
    CellState state;
    

    public CellGrid(int rows, int columns, CellState initialState) {
		this.cols = columns;
        this.rows = rows;
        this.state = initialState;
        grid = new CellState [rows][columns]; 
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (((row <= 0) && (row > rows)) && ((column <= 0) && (column > cols))){
            throw new IndexOutOfBoundsException();
        }
        else{
            grid[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (((row <= 0) && (row > rows)) && ((column <= 0) && (column > cols))){
            throw new IndexOutOfBoundsException();
        }
        else{
            return grid[row][column];
        }
    }

    @Override
    public IGrid copy() { 
    IGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
    for(int r = 0; r < rows; r ++){
        for(int c = 0; c < cols; c ++){
            newGrid.set(r, c, get(r, c));
        }
    }
        return newGrid;
    }
}
